import { Router, Request, Response, NextFunction } from 'express';
import * as messageService from '../services/messageService';
import UserCreationAttributes from '../data/modelsAttribute/user';

const router = Router();

router
  .get('/', (req: Request, res: Response, next: NextFunction) => messageService.getMessages(req.query)
    .then(messages => res.send(messages))
    .catch(next))
  .get('/:id', (req: Request, res: Response, next: NextFunction) => messageService.getMessageById(req.params.id)
    .then(message => res.send(message))
    .catch(next))
  .get('/info/:id', (req: Request, res: Response, next: NextFunction) => messageService.getReactionUsers(req.params.id, req.query)
    .then(message => res.send(message))
    .catch(next))
  .post('/', (req: Request, res: Response, next: NextFunction) => messageService.create((req.user as UserCreationAttributes).id as string, req.body)
    .then(message => {
      return res.send(message);
    })
    .catch(next))
  .put('/react', (req: Request, res: Response, next: NextFunction) => messageService.setReaction((req.user as UserCreationAttributes).id as string, req.body)
    .then(reaction => {
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req: Request, res: Response, next: NextFunction) => messageService.update(req.params.id, req.body)
    .then(message => {
      return res.send(message);
    })
    .catch(next))
  .delete('/:id', (req: Request, res: Response, next: NextFunction) => messageService.deleteMessage(req.params.id)
    .then(() => {
      return res.send({id: req.params.id});
    })
    .catch(next));

export default router;
