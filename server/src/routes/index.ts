import { Application } from 'express';
import authRoutes from './authRoutes';
import userRoutes from './userRoutes';
import messageRoutes from './messageRoutes';

// register all routes
export default (app: Application) => {
  app.use('/api/auth', authRoutes);
  app.use('/api/users', userRoutes);
  app.use('/api/messages', messageRoutes);
};
