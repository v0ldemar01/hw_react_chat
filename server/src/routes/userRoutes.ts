import { Router, Request, Response, NextFunction } from 'express';
import * as userService from '../services/userService';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .get('/', (req: Request, res: Response, next: NextFunction) => userService.getUsers()
    .then(users => res.send(users))
    .catch(next))
  .post('/', (req: Request, res: Response, next: NextFunction) => userService.createUser(req.body)
    .then(user => res.send(user))
    .catch(next))
  .get('/:id', (req: Request, res: Response, next: NextFunction) => userService.getUserById(req.params.id)
    .then(data => res.send(data))
    .catch(next))
  .put('/:id', (req: Request, res: Response, next: NextFunction) => userService.update(req.params.id, req.body)
    .then(post => res.send(post))
    .catch(next))
  .delete('/:id', (req: Request, res: Response, next: NextFunction) => userService.deleteUser(req.params.id)
    .then(() => {
      return res.send({id: req.params.id});
    })
    .catch(next));

export default router;
