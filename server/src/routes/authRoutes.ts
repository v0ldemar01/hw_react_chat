import { Router, Request, Response, NextFunction } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import UserCreationAttributes from '../data/modelsAttribute/user';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req: Request, res: Response, next: NextFunction) => authService.login(req.user as UserCreationAttributes)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req: Request, res: Response, next: NextFunction) => authService.register(req.user as UserCreationAttributes)
    .then(data => res.send(data))
    .catch(next))
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById((req.user as UserCreationAttributes).id as string)
    .then(data => res.send(data))
    .catch(next))

export default router;
