import userRepository from '../data/repositories/userRepository';
import UserCreationAttributes from '../data/modelsAttribute/user';
import { encryptSync }  from '../helpers/cryptoHelper';

export const getUsers = () => userRepository.getAll();

export const getUserById = (userId: string) => userRepository.getUserById(userId);
 
export const update = (userId: string, user: UserCreationAttributes) => 
  userRepository.updateById(userId, {...user, password: encryptSync(user.password)});

export const deleteUser = (userId: string) => userRepository.deleteById(userId);

export const createUser = (user: UserCreationAttributes) => userRepository.create({...user, isAdmin: user.isAdmin || false});