import { createToken } from '../helpers/tokenHelper';
import { encrypt } from '../helpers/cryptoHelper';
import userRepository from '../data/repositories/userRepository';
import UserCreationAttributes from '../data/modelsAttribute/user';

export const login = async ({ id }: UserCreationAttributes) => ({
  token: createToken({ id: id as string }),
  user: await userRepository.getUserById(id as string)
});

export const register = async ({ username, password, avatar, isAdmin }: UserCreationAttributes) => {
  const newUser = await userRepository.addUser({
    username,
    avatar: avatar || '',
    isAdmin: isAdmin || false,    
    password: await encrypt(password)
  } as UserCreationAttributes);
  return login(newUser as UserCreationAttributes);
};

