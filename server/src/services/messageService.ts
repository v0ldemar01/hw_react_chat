import messageRepository from '../data/repositories/messageRepository';
import messageReactionRepository from '../data/repositories/messageReactionRepository';
import IFilter from '../models/filter';
import MessageCreationAttributes from '../data/modelsAttribute/message';
import MessageReactionCreationAttributes from '../data/modelsAttribute/messageReaction';
import IReaction from '../models/reaction';

export const getMessages = (filter: IFilter) => messageRepository.getMessages(filter);

export const getMessageById = (id: string) => messageRepository.getPostById(id);

export const create = (userId: string, {text}: MessageCreationAttributes) =>
  messageRepository.create({
    text,
    userId,
    createdAt: new Date()
  } as MessageCreationAttributes);

export const update = (messageId: string, message: MessageCreationAttributes) => 
  messageRepository.updateById(messageId, {...message, updatedAt: new Date()});

export const deleteMessage = (messageId: string) => messageRepository.deleteById(messageId);

export const getReactionUsers = (messageId: string, {isLike}: IReaction) =>
  messageReactionRepository.getReactionUsers(messageId, isLike as boolean);

export const setReaction = async (userId: string, { messageId, isLike, isDisLike }: IReaction) => {
  
  
  // define the callback for future use as a promise
  const switchPostReaction = async (react: MessageReactionCreationAttributes) => {
    if ((isLike && react.isDisLike) || (isDisLike && react.isLike)) {
      await messageReactionRepository.deleteById(react.id as string);
      await messageReactionRepository.create({ userId, messageId, isLike, isDisLike } as MessageReactionCreationAttributes);
      return 'switch';
    }
  };

  const updateOrDelete = (react: MessageReactionCreationAttributes) => (
    (isLike && react.isLike) || (isDisLike && react.isDisLike)
      ? messageReactionRepository.deleteById(react.id as string)
      : switchPostReaction(react)
  );

  const reaction = await messageReactionRepository.getMessageReaction(userId as string, messageId as string);
  
  reaction
    ? await updateOrDelete(reaction as MessageReactionCreationAttributes)
    : await messageReactionRepository.create({ userId, messageId, isLike, isDisLike } as MessageReactionCreationAttributes);

  // the result is an integer when an entity is deleted
  return {messageId};
};


