import crypto from 'crypto';

export const createResetToken = () => crypto.randomBytes(20).toString('hex');
