const bcrypt = require('bcrypt');

const saltRounds = 10;

exports.encrypt = (data) => bcrypt.hash(data, saltRounds);

exports.encryptSync = (data) => bcrypt.hashSync(data, saltRounds);

exports.compare = (data, encrypted) => bcrypt.compare(data, encrypted);
