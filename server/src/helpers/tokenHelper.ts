import jwt from 'jsonwebtoken';
import { secret, expiresIn } from '../config/jwtConfig';

export const createToken = (data: {id: string}) => jwt.sign(data, secret as string, { expiresIn });
