import { Request, Response, NextFunction } from 'express';
import jwtMiddleware from './jwtMiddleware';

export default (routesWhiteList = [] as string[]) => (req: Request, res: Response, next: NextFunction) => (
  routesWhiteList.some(route => req.path.includes(route))
    ? next()
    : jwtMiddleware(req, res, next) // auth the user if requested path isn't from the white list
);
