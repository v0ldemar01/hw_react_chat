import { Request, Response, NextFunction } from 'express';
import IHttpError from '../models/error';

export default (err: IHttpError, req: Request, res: Response, next: NextFunction) => {
  if (res.headersSent) { // http://expressjs.com/en/guide/error-handling.html
    next(err);
  } else {
    const { status = 500, message = '' } = err;
    res.status(status).send({ status, message });
  }
};
