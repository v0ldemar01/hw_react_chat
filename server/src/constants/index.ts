/* eslint-disable linebreak-style */
export const HTTP_STATUS_ERROR_BAD_REQUEST = 400;
export const REGEXP_PHONE = /^\+380(\d{9})$/;
export const REGEXP_EMAIL = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail.com$/;
