
import IO from 'socket.io';

export default (io: IO.Server) => {
  io.on('connection', (socket: IO.Socket) => {
    console.log(`${socket.id} is connected`);
    socket.on('disconnect', () => {
      console.log(`${socket.id} is disconnected`);
    });
  });
};

