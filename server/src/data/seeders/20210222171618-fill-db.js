const { QueryTypes } = require('sequelize');
const { usersSeed } = require('../seed-data/usersSeed');
const { messagesSeed } = require('../seed-data/messagesSeed');

module.exports =  {
  up: async queryInterface => {
    try {
      const options = {
        type: QueryTypes.SELECT
      };

      // Add users.
      await queryInterface.bulkInsert('users', usersSeed, {});
      const users = await queryInterface.sequelize.query('SELECT id, username FROM "users";', options);

      // Add messages.
      const messagesMappedSeed = messagesSeed.map(({text, createdAt, updatedAt, username}, i) => ({
        text,
        createdAt,
        updatedAt,
        userId: users.find(user => user.username === username).id
      }));
      await queryInterface.bulkInsert('messages', messagesMappedSeed, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('messages', {}, {});
      await queryInterface.bulkDelete('users', {}, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  }
};
