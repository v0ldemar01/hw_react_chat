import UserCreationAttributes from './user';
import MessageCreationAttributes from './message';
import MessageReactionCreationAttributes from './messageReaction';

export type sequelizeModels = UserCreationAttributes | MessageCreationAttributes | MessageReactionCreationAttributes;