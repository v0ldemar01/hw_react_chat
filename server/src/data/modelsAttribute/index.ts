import UserCreationAttributes from '../modelsAttribute/user';
import MessageCreationAttributes from '../modelsAttribute/message';
import MessageReactionCreationAttributes from '../modelsAttribute/messageReaction';
import { ModelCtor } from 'sequelize/types';

export default interface IModels {
  User: ModelCtor<UserCreationAttributes>,
  Message: ModelCtor<MessageCreationAttributes>,
  MessageReaction: ModelCtor<MessageReactionCreationAttributes>
}