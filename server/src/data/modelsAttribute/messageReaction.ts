import { Model, Optional } from 'sequelize';

interface IMessageReactionModelAttribute extends Model {
  id?: string,
  isLike?: boolean,
  isDisLike?: boolean,
  createdAt?: Date,
  messageId: string,
  userId: string
}

export default interface MessageReactionCreationAttributes extends Optional<IMessageReactionModelAttribute, 'id'> {}