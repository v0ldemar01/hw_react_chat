import { Model, Optional } from 'sequelize';

export interface IUser {
  username: string;
  password: string;
  avatar: string;
  isAdmin: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IUserModelAttribute extends IUser, Model {
  id?: string;
}

export default interface UserCreationAttributes extends Optional<IUserModelAttribute, 'id'> {}