import { Model, Optional } from 'sequelize';

interface IMessageModelAttribute extends Model {
  id?: string,
  text: string,
  createdAt?: Date,
  updatedAt?: Date,
  userId?: string
}

export default interface MessageCreationAttributes extends Optional<IMessageModelAttribute, 'id'> {}