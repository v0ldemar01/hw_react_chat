import { QueryInterface, DataTypes } from 'sequelize';

export default {
  up: (queryInterface: QueryInterface)  => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('messages', 'userId', {
        type: DataTypes.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('messageReactions', 'userId', {
        type: DataTypes.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('messageReactions', 'messageId', {
        type: DataTypes.UUID,
        references: {
          model: 'messages',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
    ])),

  down: (queryInterface: QueryInterface) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('messages', 'userId', { transaction }),
      queryInterface.removeColumn('messageReactions', 'userId', { transaction }),
      queryInterface.removeColumn('messageReactions', 'messageId', { transaction }),
    ]))
};
