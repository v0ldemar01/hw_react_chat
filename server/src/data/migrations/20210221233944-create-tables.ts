import { QueryInterface, DataTypes, literal } from 'sequelize';

export default {
  up: (queryInterface: QueryInterface) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        username: {
          allowNull: false,
          type: DataTypes.STRING,
          unique: true
        },
        password: {
          allowNull: false,
          type: DataTypes.STRING
        },
        avatar: {
          allowNull: true,
          type: DataTypes.STRING
        },
        isAdmin: {
          allowNull: false,
          type: DataTypes.BOOLEAN
        },
        createdAt: {
          allowNull: true,
          type: DataTypes.DATE,
        },
        updatedAt: {
          allowNull: true,
          type: DataTypes.DATE,
          defaultValue: null
        }
      }, { transaction }),
      queryInterface.createTable('messages', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        text: {
          allowNull: false,
          type: DataTypes.STRING
        },
        createdAt: {
          allowNull: true,
          type: DataTypes.DATE,
        },
        updatedAt: {
          allowNull: true,
          type: DataTypes.DATE,
          defaultValue: null
        }
      }, { transaction }),
      queryInterface.createTable('messageReactions', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: DataTypes.UUID,
          defaultValue: literal('gen_random_uuid()')
        },
        isLike: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false
        },
        isDisLike: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false
        },
        createdAt: {
          allowNull: true,
          type: DataTypes.DATE,
        }
      }, { transaction }),
    ]))),

  down: (queryInterface: QueryInterface) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('users', { transaction }),
      queryInterface.dropTable('messages', { transaction })
    ]))
};
