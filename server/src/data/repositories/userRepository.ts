import UserCreationAttributes from '../modelsAttribute/user';
import { UserModel } from '../models/index';
import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  addUser(user: UserCreationAttributes) {
    return this.create(user);
  }

  getByUsername(username: string) {
    return this.model.findOne({
      where: { username }
    });
  }

  getUserById(id: string) {
    return this.model.findOne({
      where: { id }
    });
  }
}

export default new UserRepository(UserModel);
