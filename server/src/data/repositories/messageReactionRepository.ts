import Sequelize from 'sequelize';
import { MessageReactionModel, MessageModel, UserModel } from '../models/index';
import BaseRepository from './baseRepository';

class MessageReactionRepository extends BaseRepository {
  getMessageReaction(userId: string, messageId: string) {
    return this.model.findOne({
      group: [
        'messageReaction.id',
        'message.id'
      ],
      where: { userId, messageId },
      include: [{
        model: MessageModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getReactionUsers(messageId: string, isLike: boolean) {
    const where = {};
    Object.assign(where, {
      id: {
        [Sequelize.Op.in]: Sequelize.literal(
          `(SELECT "messageReactions"."userId" FROM "messageReactions" WHERE "messageReactions"."postId" = '${messageId}' AND `
          + `"messageReactions"."${isLike ? 'isLike' : 'isDisLike'}" = '${true}')`
        )
      }
    });
    return UserModel.findAll({
      where
    });
  }
}

export default new MessageReactionRepository(MessageReactionModel);
