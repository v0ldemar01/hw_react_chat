import Sequelize from 'sequelize';
import { MessageModel, UserModel, MessageReactionModel } from '../models/index';
import BaseRepository from './baseRepository';
import IFilter from '../../models/filter';

const likeCase = (bool: boolean) => `CASE WHEN "messageReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class MessageRepository extends BaseRepository {
  getMessages(filter: IFilter) {
    const {
      from: offset,
      count: limit,
      searchTerm
    } = filter;
    const where = {};
    if (searchTerm) {
      Object.assign(where, {
        text: {
          [Sequelize.Op.like]: `%${searchTerm}%`
        }
      });
    }
    return this.model.findAll({
      where,
      attributes: {
        include: [
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(true))), 'likeCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['username', 'avatar']
      },
      {
        model: MessageReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'message.id',
        'user.id'
      ],
      offset,
      limit
    });
  }

  getPostById(id: string) {
    return this.model.findOne({
      group: [
        'post.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(true))), 'likeCount'],
          [Sequelize.fn('SUM', Sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username', 'email']
      }]
    });
  }
}

export default new MessageRepository(MessageModel);
