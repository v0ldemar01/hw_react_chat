import { Sequelize, DataTypes } from 'sequelize';
import MessageCreationAttributes from '../modelsAttribute/message';

export default (orm: Sequelize) => {
  const Message = orm.define<MessageCreationAttributes>('message', {
    text: {
      allowNull: false,
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
  }, {});

  return Message;
};
