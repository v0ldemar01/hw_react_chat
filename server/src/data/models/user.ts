import { Sequelize, DataTypes } from 'sequelize';
import UserCreationAttributes from '../modelsAttribute/user';

export default (orm: Sequelize) => {
  const User = orm.define<UserCreationAttributes>('user', {
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    avatar: {
      allowNull: false,
      type: DataTypes.STRING
    },
    isAdmin: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    createdAt: {
      allowNull: true,
      type:  DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      type:  DataTypes.DATE
    }
  }, {});

  return User;
};
