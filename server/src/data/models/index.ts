import orm from '../db/connection';
import associate from '../db/associations';
import User from './user';
import Message from './message';
import MessageReaction from './messageReaction';

const UserWrap = User(orm);
const MessageWrap = Message(orm);
const MessageReactionWrap = MessageReaction(orm);

associate({
  User: UserWrap,
  Message: MessageWrap,
  MessageReaction: MessageReactionWrap
});

export {
  UserWrap as UserModel,
  MessageWrap as MessageModel,
  MessageReactionWrap as MessageReactionModel
};
