import { Sequelize, DataTypes } from 'sequelize';
import MessageReactionCreationAttributes from '../modelsAttribute/messageReaction';

export default (orm: Sequelize) => {
  const MessageReaction = orm.define<MessageReactionCreationAttributes>('messageReaction', {
    isLike: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isDisLike: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
  }, {});

  return MessageReaction;
};
