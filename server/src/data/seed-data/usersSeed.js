const { encryptSync } = require('../../helpers/cryptoHelper');

const hash = password => encryptSync(password);

exports.usersSeed = [{
  avatar: 'https://instagram.fdnk1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/123771095_887239091808937_834820753953839979_n.jpg?_nc_ht=instagram.fdnk1-2.fna.fbcdn.net&_nc_ohc=bbsTQtD4vJYAX_DWz7c&tp=1&oh=e522f07bd174dfa93c468521e0ca3f41&oe=60570EBB',
  username: 'voldemar',
  password: hash('1411564580'),
  createdAt: new Date(),
  isAdmin: true
},
{
  avatar: 'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
  username: 'Ruth',
  password: hash('1411564580'),
  createdAt: '2020-07-15T19:48:12.936Z',
  isAdmin: false
},
{
  avatar: 'https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng',
  username: 'Wendy',
  password: hash('1411564580'),
  createdAt: '2020-07-15T19:48:42.481Z',
  isAdmin: false
},{
  avatar: 'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ',
  username: 'Helen',
  password: hash('1411564580'),
  createdAt: '2020-07-15T19:49:33.195Z',
  isAdmin: false,
},
{
  avatar: 'https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg',
  username: 'Ben',
  password: hash('1411564580'),
  createdAt: '2020-07-15T19:50:07.708Z',
  isAdmin: false,
}];
