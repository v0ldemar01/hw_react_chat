import { Sequelize, Options } from 'sequelize';
import * as config from '../../config/dbConfig';

export default new Sequelize({...config, define: {
  timestamps: false
}} as Options);
