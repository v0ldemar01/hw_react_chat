import IModels from '../modelsAttribute';

export default (models: IModels) => {
  const {
    User,
    Message,
    MessageReaction
  } = models;

  User.hasMany(Message);
  User.hasMany(MessageReaction);

  Message.belongsTo(User);
  Message.hasMany(MessageReaction);

  MessageReaction.belongsTo(Message);
  MessageReaction.belongsTo(User);
};
