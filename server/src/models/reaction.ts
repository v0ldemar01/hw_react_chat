export default interface IReaction {
  messageId?: string,
  isLike?: boolean,
  isDisLike?: boolean
}