export default interface IUserModel  {
  id?: string,
  username: string;
  password: string;
  avatar: string;
  isAdmin: boolean;
  createdAt?: string;
}
