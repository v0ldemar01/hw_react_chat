export default interface IFilter {
  searchTerm?: string,
  from?: number,
  count?: number
}