import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { secret } from './jwtConfig';
import userRepository from '../data/repositories/userRepository';
import UserCreationAttributes from '../data/modelsAttribute/user';
import { compare } from '../helpers/cryptoHelper';

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secret
};

passport.use(
  'login',
  new LocalStrategy(async (username: string, password: string, done: Function) => {
    try {
      const user = await userRepository.getByUsername(username) as UserCreationAttributes;
      if (!user) {
        return done({ status: 401, message: 'Incorrect username.' }, false);
      }
      return await compare(password, user.password)
        ? done(null, user)
        : done({ status: 401, message: 'Passwords do not match.' }, null, false);
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(
  'register',
  new LocalStrategy(
    async (username, password: string, done: Function) => {
      try {
        return await userRepository.getByUsername(username)
          ? done({ status: 401, message: 'Username is already taken.' }, null)
          : done(null, { username, password });
      } catch (err) {
        return done(err);
      }
    }
  )
);

passport.use(new JwtStrategy(options, async ({ id }, done) => {
  try {
    const user = await userRepository.getById(id);
    return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
  } catch (err) {
    return done(err);
  }
}));
