import path from 'path';
import express, {Application} from 'express';
import bodyParser from 'body-parser'
import morgan from 'morgan';
import { createServer } from 'http';
import { Server } from 'socket.io';
import cors from 'cors';
import sequelize from './data/db/connection';
import routes from './routes';
import socketHandler from './socket';
import authorizationMiddleware from './middlewares/authorizationMiddleware';
import errorHandlerMiddleware from './middlewares/errorHandlerMiddleware';
import routesWhiteList from './config/routesWhiteListConfig';
import env from './env';
import passport from 'passport';
import './config/passportConfig';

const app = express();
const socketServer = createServer(app);
const io = new Server(socketServer);

socketHandler(io);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(passport.initialize());
app.use(passport.session())
app.use(errorHandlerMiddleware);
app.use('/api/', authorizationMiddleware(routesWhiteList));
routes(app);

socketServer.listen(env.app.port, () => {
  console.log(`Listen server on port ${env.app.port}`);
});
