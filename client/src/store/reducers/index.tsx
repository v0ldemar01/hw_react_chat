import {messageReducer} from './messageReducer';
import {messagePageReducer} from './messagePageReducer';
import {mainReducer} from './mainReducer';
import {userReducer} from './userReducer';

const rootReducer = {
  message: messageReducer,
  messagePage: messagePageReducer,
  main: mainReducer,
  user: userReducer
};

export default rootReducer;
