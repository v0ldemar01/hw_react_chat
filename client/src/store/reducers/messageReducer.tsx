
import { 
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAILURE,
  SEARCH_MESSAGES,
  SEARCH_MESSAGES_RESULT,  
  MessageActionTypes 
} from '../../actions/types/messageTypes';
import IMessageConfig from '../../models/message';
import IMessageState from '../../models/modelState/messageState';
import ISearchElement from '../../models/searchElement';
import moment from 'moment';

const initialState: IMessageState = { 
  messages: [] as IMessageConfig[],
  filterMessages: [] as IMessageConfig[],  
  search: {} as ISearchElement,
  error: '' 
}

export const messageReducer = (
  state = initialState,
  action: MessageActionTypes
) => {
  switch (action.type) {
    case FETCH_MESSAGES_SUCCESS: {
      return {...state, messages: action.payload
        .filter(message => message.userId)
        .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))};
    }
    
    case FETCH_MESSAGES_FAILURE: {
      return {...state, error: action.payload};
    }
    
    case SEARCH_MESSAGES: {
      return {...state, search: action.payload}
    }

    case SEARCH_MESSAGES_RESULT: {
      return {...state, filterMessages: action.payload
        .filter(message => message.userId)
        .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))}
    }
        
    default:
      return state;
  }
}