import { 
  SHOW_PAGE,
  HIDE_PAGE,
  MessagePageActionTypes 
} from '../../actions/types/messagePageTypes';
import IMessagePageState from '../../models/modelState/messagePageState';

const initialState: IMessagePageState = {
  isShown: false
};

export const messagePageReducer = (
  state = initialState, 
  action: MessagePageActionTypes
) => {
  switch (action.type) {
    case SHOW_PAGE: {
      return {
        ...state,
        isShown: true
      };
    }

    case HIDE_PAGE: {
      return {
        ...state,
        isShown: false
      };
    }

    default:
      return state;
  }
}
