
import { 
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,  
  UserActionTypes, 
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILURE,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE
} from '../../actions/types/userTypes';
import IUser from '../../models/user';
import IUserState from '../../models/modelState/userState';

const initialState: IUserState = {
  currentUser: {} as IUser,
  isLoggedIn: false,
  users: [] as IUser[],
  error: ''
}

export const userReducer = (
  state = initialState,
  action: UserActionTypes
) => {
  switch (action.type) {
    case LOAD_USER_SUCCESS:
    case LOGIN_SUCCESS: 
    case REGISTER_SUCCESS: {
      return {...state, currentUser: action.payload, isLoggedIn: true};
    }

    case LOAD_USER_FAILURE:
    case LOGIN_FAILURE: 
    case REGISTER_FAILURE: {
      return {...state, error: action.payload, isLoggedIn: false};
    }    
    
    case FETCH_USERS_SUCCESS: {
      return {...state, users: action.payload};
    }

    case FETCH_USERS_FAILURE: {
      return {...state, error: action.payload};
    }

    default:
      return state;
  }
}