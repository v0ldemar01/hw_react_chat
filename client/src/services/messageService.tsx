import IMessageConfig from '../models/message';

class MessageService {
  private _apiBase;

  constructor() {
    this._apiBase = 'https://edikdolynskyi.github.io/react_sources';
  }
  getAllMessages = async () : Promise<IMessageConfig[]> => {
    const res: IMessageConfig[] = await this.getResource(`/messages.json`);
    return res;
  }

  getResource = async (url: string) => {
    const res = await fetch(`${this._apiBase}${url}`);

    if (!res.ok) {
      throw new Error(`Could not fetch ${url}` +
        `, received ${res.status}`);
    }
    return await res.json();
  }
}

export default new MessageService();