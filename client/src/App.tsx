import React from 'react';
import Header from './components/Header';
import Chat from './components/Chat';

const App: React.FunctionComponent = () => (
  <>      
    <Header />
    <Chat />
  </>
);  

export default App;
