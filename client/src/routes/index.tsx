import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {Switch, Redirect} from 'react-router-dom';
import IState from '../models/modelState';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import MessagePage from '../pages/MessagePage';
import UserPage from '../pages/UserPage';
import App from '../App';
import UserList from '../components/UserList';
import Spinner from '../components/Spinner';
import {loadUser, fetchUsers} from '../actions/userActions';

import '../App.css';
import 'semantic-ui-css/semantic.min.css';

const AppRouter: React.FunctionComponent = () => {
  const dispatch = useDispatch();
  const isAdmin = useSelector((state: IState) => state.user.currentUser.isAdmin);
  const isLoading = useSelector((state: IState) => state.main.isLoading);
  useEffect(() => {
    dispatch(loadUser());
  }, []); 
  
  return isLoading ? (
    <Spinner />
  ) : (
    <Switch>   
      <PublicRoute exact path='/login' component={LoginPage} />
      <PublicRoute exact path='/register' component={RegisterPage} />
      <PrivateRoute exact path='/' component={App} />
      <PrivateRoute exact path='/message/:id' component={MessagePage} />
      {isAdmin && <PrivateRoute exact path='/users' component={UserList} />}
      {isAdmin && <PrivateRoute exact path='/user/:id' component={UserPage} />}
      {/* <Redirect from="*" to="/"/>       */}
    </Switch>
  )
}

export default AppRouter;