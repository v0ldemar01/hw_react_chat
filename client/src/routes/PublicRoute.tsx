import * as React from 'react';
import { useSelector } from 'react-redux';
import {
  Route,
  Redirect,
  RouteProps,
} from 'react-router-dom';
import IRouteProps from '../models/modelProps/routeProps';
import IState from '../models/modelState';

const PrivateRoute: React.FunctionComponent<IRouteProps> = ({
  component: Component,
  ...rest
}: IRouteProps) => {
  const isSignedIn = useSelector((state: IState) => state.user.isLoggedIn);
  const isAdmin = useSelector((state: IState) => state.user.currentUser.isAdmin);
    return (
      <Route
        {...rest}
        render={(routeProps: RouteProps) =>
          isSignedIn ? (
            <Redirect
              to={{
                  pathname: isAdmin ? '/users' : '/',
                  state: { from: routeProps.location }
              }}
            />            
          ) : (
            <Component {...routeProps} />
          )
        }
      />
    );
};

export default PrivateRoute;