import * as React from 'react';
import { useSelector } from 'react-redux';
import {
  Route,
  Redirect,
  RouteProps,
} from 'react-router-dom';
import IRouteProps from '../models/modelProps/routeProps';
import IState from '../models/modelState';

const PrivateRoute: React.FunctionComponent<IRouteProps> = ({
  component: Component,
  ...rest
}: IRouteProps) => {
  const isSignedIn = useSelector((state: IState) => state.user.isLoggedIn);
  console.log('isSignedIn', isSignedIn);
    return (
      <Route
        {...rest}
        render={(routeProps: RouteProps) =>
          isSignedIn ? (
            <Component {...routeProps} />
          ) : (
                <Redirect
                  to={{
                    pathname: '/login',
                    state: { from: routeProps.location }
                  }}
                />
          )
        }
      />
    );
};

export default PrivateRoute;