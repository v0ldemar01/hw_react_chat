import IHeaders from '../models/headers';

export const getHeaders = () => {
  const headers = {} as IHeaders;
  const token = localStorage.getItem('token');
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }
  return headers;
}