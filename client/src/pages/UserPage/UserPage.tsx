import React, {useState} from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { editUser, addUser } from '../../actions/userActions';
import { RouteComponentProps } from 'react-router-dom';
import IState from '../../models/modelState';
import IUser from '../../models/user';
import { Grid, Header, Icon, Form, Segment, Button} from 'semantic-ui-react';

interface RouteParams {
  id: string
}

const UserPage: React.FunctionComponent<RouteComponentProps<RouteParams> & PropsFromRedux> = ({
  editUser: edit,
  addUser: add,
  users,
  match,
  history
}: RouteComponentProps<RouteParams> & PropsFromRedux) => {
  const id = match.params.id;  
  
  const thisUser = id === 'new' ? {} : (users as IUser[]).find(user => user.id === id);

  const [username, setUsername] = useState((thisUser as IUser).username || '');
  const [password, setPassword] = useState('');
  const [avatar, setAvatar] = useState((thisUser as IUser).avatar || '');

  const hangleAction = () => {
    const user = {...thisUser, username, password, avatar};
    if (id === 'new') {
      add(user as IUser);
    } else {
      edit(user as IUser);
    }
   
    history.push('/users');
  }

  return ( 
    <Grid textAlign="center" verticalAlign="middle" className="app">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h1" icon color="orange" textAlign="center">
          <Icon name="chat" color="orange" />
        </Header>
        <Form onSubmit={hangleAction} size="large">
          <Segment stacked>
            <Form.Input
              fluid
              name="username"
              icon="user"
              iconPosition="left"
              placeholder="Username"
              onChange={event => setUsername(event.target.value)}
              value={username}                
              type="username"
            />
            <Form.Input
              fluid
              name="avatar"
              icon="user circle"
              iconPosition="left"
              placeholder="Avatar"
              onChange={event => setAvatar(event.target.value)}
              value={avatar}               
            />
            <Form.Input
              fluid
              name="password"
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              onChange={event => setPassword(event.target.value)}
              value={password}                
              type="password"
            />
            <Button.Group widths="2">
              <Button                
                negative
                onClick={() => history.push('/users')}                
              >
                Cancel
              </Button>
              <Button 
                color="violet"               
                disabled={!(username && password)} 
              >  
                Submit
              </Button>
            </Button.Group>          
          </Segment>
        </Form>         
      </Grid.Column>
    </Grid>
  );
}

const mapStateToProps = (state: IState) => ({
  users: state.user.users
});

const mapDispatchToProps = {
  editUser,
  addUser
}

const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(UserPage);
