import React, {useState, useRef} from 'react';
import { connect } from 'react-redux'
import {loginInit} from '../../actions/userActions';
import { 
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon
} from 'semantic-ui-react';
import { NavLink } from "react-router-dom";
import ILoginPageProps from '../../models/modelProps/loginPageProps';

const LoginPage: React.FunctionComponent<ILoginPageProps> = ({
  loginInit: login
}: ILoginPageProps) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  
  const hangleLogin = () => {
    login({username, password});
  }

  return ( 
    <Grid textAlign="center" verticalAlign="middle" className="app">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h1" icon color="orange" textAlign="center">
          <Icon name="chat" color="orange" />
          Login to Chat
        </Header>
        <Form onSubmit={hangleLogin} size="large">
          <Segment stacked>
            <Form.Input
              fluid
              name="username"
              icon="user"
              iconPosition="left"
              placeholder="Username"
              onChange={event => setUsername(event.target.value)}
              value={username}                
              type="username"
            />
            <Form.Input
              fluid
              name="password"
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              onChange={event => setPassword(event.target.value)}
              value={password}                
              type="password"
            />
            <Button                
              color="violet"
              fluid
              size="large"
              disabled={!(username && password)}
            >
              Submit
            </Button>
          </Segment>
        </Form>         
        <Message>
          Don't have an account? <NavLink to="/register">Register</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
}

const mapDispatchToProps = {
  loginInit
};

export default connect(null, mapDispatchToProps)(LoginPage);
