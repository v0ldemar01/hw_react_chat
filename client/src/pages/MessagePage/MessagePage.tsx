import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { editMessage } from '../../actions/messageActions';
import IState from '../../models/modelState';
import { RouteComponentProps } from 'react-router-dom';

import { Button, Form, Segment } from 'semantic-ui-react';
import IMessageConfig from '../../models/message';

interface RouteParams {
  id: string
}

const MessagePage: React.FunctionComponent<RouteComponentProps<RouteParams> & PropsFromRedux> = ({
  editMessage: edit,
  messages,
  match,
  history
}: RouteComponentProps<RouteParams> & PropsFromRedux) => {
  
  const id = match.params.id;  
  const thisMessage = messages.find(message => message.id === id);
  const [text, setText] = useState((thisMessage as IMessageConfig).text);

  const handleEditMessage = () => {
    const editedMessage = {...thisMessage, text}
    edit(editedMessage as IMessageConfig);
    history.push('/');
  } 

  return ( 
    <Segment>
      <Form onSubmit={handleEditMessage}>
        <h3>Message Editing</h3>
          <Form.TextArea 
            value={text}
            placeholder={text}            
            onChange={event => setText(event.target.value)}
            rows={10}
          />
          <Button negative onClick={() => {}}>
            Nope
          </Button>
          <Button
            content="Edit message"
            labelPosition='right'
            icon='checkmark'
            onClick={() => {}}
            positive
            disabled={text === ''}
          />
        </Form> 
    </Segment>            
  );
};

const mapStateToProps = (state: IState) => ({
  messages: state.message.messages
});

const mapDispatchToProps = {
  editMessage
}

const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(MessagePage);
