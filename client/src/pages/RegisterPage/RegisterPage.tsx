import React, {useState} from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { registerInit } from '../../actions/userActions';
import IUser from '../../models/user';
import { NavLink } from 'react-router-dom';
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon
} from 'semantic-ui-react';


const RegisterPage: React.FunctionComponent<PropsFromRedux> = ({
  registerInit: register,
}: PropsFromRedux) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const isFormEmpty = () => (
    !username.length ||      
    !password.length ||
    !confirmPassword.length
  );

  const isPasswordValid = () => password !== confirmPassword;

  const handleCreateUser = () => {
    register({username, password} as IUser);
  }

  return (
    <Grid textAlign="center" verticalAlign="middle" className="app">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h1" icon color="orange" textAlign="center">
          <Icon name="chat" color="orange" />
          Register to Chat
        </Header>
        <Form onSubmit={() => handleCreateUser()} size="large">
          <Segment stacked>
            <Form.Input
              fluid
              name="username"
              icon="user"
              iconPosition="left"
              placeholder="Username"
              onChange={event => setUsername(event.target.value)}
              value={username}
              type="text"
            />            
            <Form.Input
              fluid
              name="password"
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              onChange={event => setPassword(event.target.value)}
              value={password}              
              type="password"
            />
            <Form.Input
              fluid
              name="passwordConfirmation"
              icon="repeat"
              iconPosition="left"
              placeholder="Password Confirmation"
              onChange={event => setConfirmPassword(event.target.value)}
              value={confirmPassword}              
              type="password"
            />
            <Button              
              color="orange"
              fluid
              size="large"
              disabled={isFormEmpty() || isPasswordValid()}
            >
              Submit
            </Button>
          </Segment>
        </Form>        
        <Message>
          Already a user? <NavLink to="/login">Login</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
}

const mapDispatchToProps = {
  registerInit
}

const connector = connect(null, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(RegisterPage);
