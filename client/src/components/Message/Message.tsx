import React, {useState} from 'react';
import moment from 'moment';
import IMessageConfig from '../../models/message';
import IUser from '../../models/user';
import IMessageProps from '../../models/modelProps/messageProps';
import { Comment as MessageUI, Image, Icon } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const isOwnMessage = (messageConfig: IMessageConfig, user: IUser) => {
  return messageConfig.userId === user.id ? 'message__self' : '';
};

const timeFromNow = (timestamp: string) => moment(timestamp).fromNow();

const Message: React.FunctionComponent<IMessageProps> = ({
  message, 
  currentUser,  
  likeMessage,
  dislikeMessage,
  deleteMessage,  
  isLast
}: IMessageProps) => {
  const [showGear, setShowGear] = useState(false);
  const { id, likeCount, dislikeCount } = message;   

  const itsMe = message.userId === currentUser.id;
  return (
    <MessageUI className={isOwnMessage(message, currentUser)} style={{paddingRight: '15px'}}>
     { !itsMe && <MessageUI.Avatar src={message.user.avatar} />}
      <MessageUI.Content style={{maxWidth: '50%'}}>
        <MessageUI.Author as='a'>{message.user.username}</MessageUI.Author>
        {!message.updatedAt && <MessageUI.Metadata>{timeFromNow(message.createdAt)}</MessageUI.Metadata>}
        {message.updatedAt && <MessageUI.Metadata>{'edited ' + timeFromNow(message.updatedAt)}</MessageUI.Metadata>}
        <MessageUI.Text>{message.text}</MessageUI.Text>
        <MessageUI.Actions>
          {!itsMe && JSON.parse(likeCount as string) !== 0 && (
            <MessageUI.Action>
              <Image
                circular
                size="mini"
                style={{ width: '12px' }}
                src={currentUser.avatar}
              />
            </MessageUI.Action>
          )}
          {!itsMe && <MessageUI.Action onClick={() => likeMessage({messageId: id})}>
            <Icon name="thumbs up" style={{ marginRight: '0.75em' }} />
            {likeCount}
          </MessageUI.Action>}
            {!itsMe && JSON.parse(dislikeCount as string) !== 0 && (
              <MessageUI.Action>
                <Image
                  circular
                  size="mini"
                  style={{ width: '12px' }}
                  src={currentUser.avatar}
                />
              </MessageUI.Action>
            )}
            {!itsMe && <MessageUI.Action onClick={() => dislikeMessage({messageId: id})}>
              <Icon name="thumbs down" style={{ marginRight: '0.75em' }} />
              {dislikeCount}
            </MessageUI.Action>}
            <MessageUI.Action>
              Reply
            </MessageUI.Action>
            {itsMe && (
              <>                
                <MessageUI.Action onClick={() => deleteMessage(id)}>
                  Delete
                </MessageUI.Action>
              </>
            )}
          </MessageUI.Actions> 
          {itsMe && isLast && (
            <NavLink to={`/message/${id}`} >
              <Icon 
                className='edit-button'
                size='big' 
                name="sun outline" 
                style={{position: 'relative', top: '-52px', left: '80%', zIndex: 100}} 
                onMouseEnter={() => setShowGear(true)}
                onMouseLeave={() => setShowGear(false)}              
              />
           </NavLink>
         
          )}
      </MessageUI.Content>
    </MessageUI>
  );
} 

export default Message;
