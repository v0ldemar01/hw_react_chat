import React from 'react';
import IUserProps from '../../models/modelProps/userProps';
import { Card, Button, Image } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const User: React.FunctionComponent<IUserProps> = ({
  user,  
  deleteUser
}: IUserProps) => {
  const { id } = user;
  return (
    <Card className="user-card">
      <Card.Content>       
        <Card.Description>
          <Image
            floated='left'
            size='mini'
            src={user.avatar}
          />
          <div><h2>{user.username}</h2></div>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>        
        <Button.Group>
          <NavLink to={`/user/${id}`} >
            <Button positive >Edit</Button>
          </NavLink>          
          <Button.Or />
          <Button negative onClick={() => deleteUser(id)}>Delete</Button>
        </Button.Group>
      </Card.Content>
    </Card>    
  )
}

export default User;