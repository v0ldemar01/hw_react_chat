import React, {useState, useEffect} from 'react';
import { connect, ConnectedProps } from 'react-redux';
import ChatHeader from '../ChatHeader';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import IMessageConfig from '../../models/message';
import IState from '../../models/modelState';
import { 
  addMessage,
  editMessage,
  searchMessages  
} from '../../actions/messageActions';
import { Grid } from 'semantic-ui-react';

const Chat: React.FunctionComponent<PropsFromRedux> = ({  
  myUser,
  search, 
  messages,
  filterMessages,
  addMessage: add,
  searchMessages: searchTo  
}: PropsFromRedux) => {
  const [userCount, setUserCount] = useState(1);
  const [messagesCount, setMessagesCount] = useState(0);  
  const [lastMessageTime, setLastMessageTime] = useState('');  

  useEffect(() => {
    changeCountUsers(messages);
    changeCountMessages(messages);
    findLastTimeMessage(messages);    
  }, [messages.length]);

  const findLastTimeMessage = (messages: IMessageConfig[]) => {
    if (!messages.length) return;    
    const lastMessage = [...messages].pop();
    setLastMessageTime((lastMessage as IMessageConfig).createdAt);    
  }

  const changeCountUsers = (messages: IMessageConfig[]) => {
    const uniqueUsers = messages.reduce((acc, cur) => {
      if (!acc.includes(cur.userId)) {
        acc.push(cur.userId);
      }
      return acc;
    }, [] as string[]);
    setUserCount(uniqueUsers.length);
  };

  const changeCountMessages = (messages: IMessageConfig[]) => {
    setMessagesCount(messages.length);
  };

  const handleNewMessage = (newMessage: IMessageConfig) => {
    add(newMessage);    
  } 

  return (
    <Grid centered columns={1} className="app" style={{ background: "#B019B4" }}>
      <Grid.Column style={{margin: "50px 15%"}}>
        <ChatHeader 
          channelName="My Chat"
          searchLoading={search.searchLoading}
          handleSearchChange={searchTo}  
          usersCount={userCount}
          messagesCount={messagesCount}
          lastMessageTime={lastMessageTime}
        />
        <MessageList 
          myUser={myUser}
          messages={search.searchTerm ? filterMessages : messages}                  
        />
        <MessageInput 
          myUser={myUser}
          handleNewMessage={handleNewMessage}
        />
      </Grid.Column>   
    </Grid>
  )
}

const mapStateToProps = (state: IState) => {
	return {
		messages: state.message.messages,
    filterMessages: state.message.filterMessages,
    myUser: state.user.currentUser,
    search: state.message.search
	}
};

const mapDispatchToProps = {
  addMessage,
  editMessage,
  searchMessages  
}

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(Chat);
