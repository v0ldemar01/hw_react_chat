import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { editUser, deleteUser } from '../../actions/userActions';
import {
	fetchMessages
} from '../../actions/messageActions';
import IState from '../../models/modelState';
import User from '../User';
import { Card, Segment, Button } from 'semantic-ui-react';
import IUser from '../../models/user';
import { NavLink } from 'react-router-dom';

const UserList: React.FunctionComponent<PropsFromRedux> = ({
  users,
  editUser: editThisUser,
  deleteUser: deleteThisUser,
  fetchMessages: fetchMessagesFrom  
}) => { 

  return (
    <Segment>     
      <NavLink to="/user/new" >
      <Button style={{marginBottom: '10px'}} content="Add user" />
      </NavLink>       
      <Card.Group>
        {(users as IUser[]).map((user, index) => (
          <User 
            key={index}
            user={user}
            editUser={editThisUser}
            deleteUser={deleteThisUser}
          />
        ))}        
      </Card.Group> 
      <NavLink to="/" >
        <Button style={{marginTop: '10px'}} content="To Chat" onClick={() => fetchMessagesFrom()}/>
      </NavLink>      
    </Segment>    
  )
}

const mapStateToProps = (state: IState) => ({
  users: state.user.users
});

const mapDispatchToProps = {  
  editUser,
  deleteUser,
  fetchMessages
}

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(UserList);
