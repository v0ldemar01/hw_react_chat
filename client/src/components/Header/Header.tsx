import React from 'react';
import { NavLink } from 'react-router-dom';
import { Image, Menu, Icon } from 'semantic-ui-react';

const Header: React.FunctionComponent = () => {
  return (
    <Menu fixed="top" >
      <Menu.Item>
        <Image size="mini" src="https://i.pinimg.com/originals/45/30/45/453045f47c6274e3a728bb6d530bb8db.png" />
      </Menu.Item>   
      <Menu.Item position='right'>
        <NavLink to="/users" >
          <Icon size="big" name="user secret" />
        </NavLink>        
      </Menu.Item>   
    </Menu>
  );
}

export default Header;