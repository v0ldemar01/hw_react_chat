import React, {useEffect, useRef } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import Message from '../Message';
import IMessageListProps from '../../models/modelProps/messageListProps';
import IMessageConfig from '../../models/message';
import IState from '../../models/modelState';
import { 
  likeMessage,
  dislikeMessage,
  deleteMessage  
} from '../../actions/messageActions';
import {
  showPage,
  hidePage
} from '../../actions/messagePageActions';
import { Segment, Comment } from 'semantic-ui-react';

const MessageList: React.FunctionComponent<IMessageListProps & PropsFromRedux> = ({
  myUser,
  messages,
  likeMessage: like,
  dislikeMessage: dislike,
  deleteMessage: deleteMessageFrom  
}: IMessageListProps & PropsFromRedux) => {    
  const messagesEnd = useRef<HTMLInputElement>(null);  
    
  useEffect(() => {
    messagesEnd.current && messagesEnd.current.scrollIntoView({ behavior: 'smooth' });
  }, [messages.length]);     

  const displayMessages = (messages: IMessageConfig[]) =>
    messages.length > 0 &&
    messages.map((message, index) => (
      <Message
        key={index}
        message={message}
        currentUser={myUser}
        likeMessage={like}
        dislikeMessage={dislike}
        deleteMessage={deleteMessageFrom}
        isLast={index === messages.length - 1}
      />      
    ));  
  
  return (
    <Segment>
      <Comment.Group className="messages" style={{maxWidth: '100%'}}>            
        {displayMessages(messages)}            
        <div ref={messagesEnd} />                
      </Comment.Group>
    </Segment>
  );
}

const mapStateToProps = (state: IState) => {
  return {
    isShown: state.messagePage.isShown,
    editingMessage: state.message
  }  
}

const mapDispatchToProps = {
  likeMessage,
  dislikeMessage,
  deleteMessage,  
  showPage,
  hidePage
}

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(MessageList);
