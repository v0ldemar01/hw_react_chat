import React, {useEffect, useState} from 'react';
import ISearchElement from '../../models/searchElement'; 
import IChatHeaderProps from '../../models/modelProps/chatHeaderProps';
import { Header, Segment, Input, Icon } from 'semantic-ui-react';
import moment from 'moment';

const ChatHeader: React.FunctionComponent<IChatHeaderProps> = ({
  channelName,
  handleSearchChange,
  searchLoading,
  usersCount,
  messagesCount,
  lastMessageTime
}: IChatHeaderProps) => {
  const [isStarred, setStarred] = useState(false);
  useEffect(() => {}, [usersCount, messagesCount]);

  const handleStar = () => setStarred(!isStarred);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    handleSearchChange({
      searchTerm: (event.target as HTMLInputElement).value      
    } as ISearchElement);
  }   
  
  const toString = (count: number, characteristic: string) => 
    `${count} ${characteristic}` + (count > 1 ? 's' : '');
  
  const timeFromNow = (timestamp: string) => moment(timestamp).fromNow();
  
  return (
    <Segment clearing>
      <Header fluid="true" as="h2" floated="left" style={{ marginBottom: 0 }} size='medium'>
        <span>
          {channelName}
          <Icon
            onClick={handleStar}
            name={isStarred ? "star" : "star outline"}
            color={isStarred ? "yellow" : "black"}
          />
        </span>
        <Header.Subheader>
          <span>{toString(usersCount, 'participant')}</span> 
          {' '}
          <span>{toString(messagesCount, 'message')} </span>
        </Header.Subheader>
      </Header>   
      <Header fluid="true" as="h2" floated="left" style={{ marginBottom: 0, marginLeft: '3em' }} size='medium'>
        <Input            
          size="mini"
          icon="search"
          name="searchTerm"
          placeholder="Search Messages"
          loading={searchLoading}
          onChange={handleChange}
        />
      </Header>        
      <Header floated="right" as="h4">
        <span>{'last message at ' + timeFromNow(lastMessageTime)}</span> 
      </Header>
    </Segment>
  );
}

export default ChatHeader;