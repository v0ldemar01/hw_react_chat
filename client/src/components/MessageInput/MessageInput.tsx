import React, {useState} from 'react';
import IMessageInputProps from '../../models/modelProps/messageInputProps';
import { Segment, Form } from 'semantic-ui-react';
import IMessageConfig from '../../models/message';

const MessageInput: React.FunctionComponent<IMessageInputProps> = ({
  myUser,
  handleNewMessage
}: IMessageInputProps) => {
  const [inputMessage, setInputMessage] = useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputMessage(event.target.value);
  };

  const createMessage = (messageText: string) => {
    const message = {
      user: {
        id: myUser.id, 
        username: myUser.username as string,
        avatar: myUser.avatar
      },      
      text: messageText      
    };    
    return message;
  };

  const sendMessage = () => {
    if (!inputMessage) return;
    const newMessage: any = createMessage(inputMessage);
    handleNewMessage(newMessage);
    setInputMessage('');
  };  
  return (
    <Segment style={{paddingBottom: 0}}> 
      <Form onSubmit={sendMessage}>
        <Form.Group>
          <Form.Input     
            width={14}   
            name="message"
            onChange={handleChange}
            value={inputMessage}          
            labelPosition="left"          
            placeholder="Write your message"
          />
          <Form.Button 
            color="orange"
            content="Send"
            labelPosition="left"
            icon="send"
          />
        </Form.Group>
      </Form>
    </Segment> 
  );
}

export default MessageInput;

