import { 
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAILURE,
  ADD_MESSAGE,
  ADD_MESSAGE_SUCCESS,
  ADD_MESSAGE_FAILURE,
  SEARCH_MESSAGES,
  SEARCH_MESSAGES_RESULT,
  SEARCH_MESSAGES_FAILURE,
  EDIT_MESSAGE,
  EDIT_MESSAGE_SUCCESS,
  EDIT_MESSAGE_FAILURE,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_FAILURE,
  LIKE_MESSAGE,
  LIKE_MESSAGE_SUCCESS,
  LIKE_MESSAGE_FAILURE,
  DISLIKE_MESSAGE,
  DISLIKE_MESSAGE_SUCCESS,
  DISLIKE_MESSAGE_FAILURE,
  MessageActionTypes
} from './types/messageTypes';
import IMessageConfig from '../models/message';
import IMessageReact from '../models/react';
import ISearchElement from '../models/searchElement';
import IHttpError from '../models/error';

export const fetchMessages = (): MessageActionTypes => ({
  type: FETCH_MESSAGES  
});

export const fetchMessagesSuccess = (messages: IMessageConfig[]): MessageActionTypes => ({
  type: FETCH_MESSAGES_SUCCESS,
  payload: messages
});

export const fetchMessagesFailure = (error: IHttpError): MessageActionTypes => ({
  type: FETCH_MESSAGES_FAILURE,
  payload: error
});

export const addMessage = (message: IMessageConfig): MessageActionTypes => ({
  type: ADD_MESSAGE,
  payload: message
});

export const addMessageSuccess = (): MessageActionTypes => ({
  type: ADD_MESSAGE_SUCCESS
});

export const addMessageFailure = (error: IHttpError): MessageActionTypes => ({
  type: ADD_MESSAGE_FAILURE,
  payload: error
});

export const searchMessages = (search: ISearchElement): MessageActionTypes => ({
  type: SEARCH_MESSAGES,
  payload: search
});

export const searchMessagesResult = (messages: IMessageConfig[]): MessageActionTypes => ({
  type: SEARCH_MESSAGES_RESULT,
  payload: messages
});

export const searchMessagesFailure = (error: IHttpError): MessageActionTypes => ({
  type: SEARCH_MESSAGES_FAILURE,
  payload: error
});

export const editMessage = (message: IMessageConfig): MessageActionTypes => ({
  type: EDIT_MESSAGE,
  payload: message
});

export const editMessageSuccess = (): MessageActionTypes => ({
  type: EDIT_MESSAGE_SUCCESS  
});

export const editMessageFailure = (error: IHttpError): MessageActionTypes => ({
  type: EDIT_MESSAGE_FAILURE,
  payload: error
});

export const deleteMessage = (id: string): MessageActionTypes => ({
  type: DELETE_MESSAGE,
  payload: id
});

export const deleteMessageSuccess = (): MessageActionTypes => ({
  type: DELETE_MESSAGE_SUCCESS
});

export const deleteMessageFailure = (error: IHttpError): MessageActionTypes => ({
  type: DELETE_MESSAGE_FAILURE,
  payload: error
});

export const likeMessage = (message: IMessageReact): MessageActionTypes => ({
  type: LIKE_MESSAGE,
  payload: message
});

export const likeMessageSuccess = (): MessageActionTypes => ({
  type: LIKE_MESSAGE_SUCCESS
});

export const likeMessageFailure = (error: IHttpError): MessageActionTypes => ({
  type: LIKE_MESSAGE_FAILURE,
  payload: error
});

export const dislikeMessage = (message: IMessageReact): MessageActionTypes => ({
  type: DISLIKE_MESSAGE,
  payload: message
});

export const dislikeMessageSuccess = (): MessageActionTypes => ({
  type: DISLIKE_MESSAGE_SUCCESS
});

export const dislikeMessageFailure = (error: IHttpError): MessageActionTypes => ({
  type: DISLIKE_MESSAGE_FAILURE,
  payload: error
});

