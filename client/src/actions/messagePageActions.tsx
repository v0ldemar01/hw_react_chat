import { 
  SHOW_PAGE,
  HIDE_PAGE,
  MessagePageActionTypes 
} from './types/messagePageTypes';

export const showPage = (): MessagePageActionTypes => ({
  type: SHOW_PAGE
});

export const hidePage = (): MessagePageActionTypes => ({
  type: HIDE_PAGE
});