import { 
  LOAD_USER,
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILURE,
  LOGIN_INIT,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  REGISTER_INIT,
  REGISTER_SUCCESS,
  REGISTER_FAILURE, 
  ADD_USER,
  ADD_USER_SUCCESS, 
  ADD_USER_FAILURE, 
  EDIT_USER,
  EDIT_USER_SUCCESS, 
  EDIT_USER_FAILURE, 
  DELETE_USER, 
  DELETE_USER_SUCCESS, 
  DELETE_USER_FAILURE,
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  UserActionTypes,
} from './types/userTypes';
import IUser from '../models/user';
import IHttpError from '../models/error';

export const loadUser = (): UserActionTypes => ({
  type: LOAD_USER  
});

export const loadUserSuccess = (user: IUser): UserActionTypes => ({
  type: LOAD_USER_SUCCESS,
  payload: user
});

export const loadUserFailure = (error: IHttpError): UserActionTypes => ({
  type: LOAD_USER_FAILURE,
  payload: error
});

export const loginInit = (user: IUser): UserActionTypes => ({
  type: LOGIN_INIT,
  payload: user
});

export const loginSuccess = (user: IUser): UserActionTypes => ({
  type: LOGIN_SUCCESS,
  payload: user
});

export const loginFailure = (error: IHttpError): UserActionTypes => ({
  type: LOGIN_FAILURE,
  payload: error
});

export const registerInit = (user: IUser): UserActionTypes => ({
  type: REGISTER_INIT,
  payload: user
});

export const registerSuccess = (user: IUser): UserActionTypes => ({
  type: REGISTER_SUCCESS,
  payload: user
});

export const registerFailure = (error: IHttpError): UserActionTypes => ({
  type: REGISTER_FAILURE,
  payload: error
});

export const addUser = (user: IUser): UserActionTypes => ({
  type: ADD_USER,
  payload: user
});

export const addUserSuccess = (): UserActionTypes => ({
  type: ADD_USER_SUCCESS 
});

export const addUserFailure = (error: IHttpError): UserActionTypes => ({
  type: ADD_USER_FAILURE,
  payload: error
});

export const editUser = (user: IUser): UserActionTypes => ({
  type: EDIT_USER,
  payload: user
});

export const editUserSuccess = (): UserActionTypes => ({
  type: EDIT_USER_SUCCESS 
});

export const editUserFailure = (error: IHttpError): UserActionTypes => ({
  type: EDIT_USER_FAILURE,
  payload: error
});

export const deleteUser = (id: string): UserActionTypes => ({
  type: DELETE_USER,
  payload: id
});

export const deleteUserSuccess = (): UserActionTypes => ({
  type: DELETE_USER_SUCCESS 
});

export const deleteUserFailure = (error: IHttpError): UserActionTypes => ({
  type: DELETE_USER_FAILURE,
  payload: error
});

export const fetchUsers = (): UserActionTypes => ({
  type: FETCH_USERS 
});

export const fetchUsersSuccess = (users: IUser[]): UserActionTypes => ({
  type: FETCH_USERS_SUCCESS,
  payload: users
});

export const fetchUsersFailure = (error: IHttpError): UserActionTypes => ({
  type: FETCH_USERS_FAILURE,
  payload: error
});


