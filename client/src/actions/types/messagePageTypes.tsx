export const SHOW_PAGE = 'SHOW_PAGE';
export const HIDE_PAGE = 'HIDE_PAGE';

interface IShowPageAction {
  type: typeof SHOW_PAGE  
}

interface IHidePageAction {
  type: typeof HIDE_PAGE  
}

export type MessagePageActionTypes = 
  IShowPageAction |
  IHidePageAction;