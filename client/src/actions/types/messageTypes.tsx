import IHttpError from '../../models/error';
import IMessageConfig from '../../models/message';
import IMessageReact from '../../models/react';
import ISearchElement from '../../models/searchElement';

export const FETCH_MESSAGES = 'FETCH_MESSAGES';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const ADD_MESSAGE_SUCCESS = 'ADD_MESSAGE_SUCCESS';
export const ADD_MESSAGE_FAILURE = 'ADD_MESSAGE_FAILURE';
export const SEARCH_MESSAGES = 'SEARCH_MESSAGES';
export const SEARCH_MESSAGES_RESULT = 'SEARCH_MESSAGES_RESULT';
export const SEARCH_MESSAGES_FAILURE = 'SEARCH_MESSAGES_FAILURE';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const EDIT_MESSAGE_SUCCESS = 'EDIT_MESSAGE_SUCCESS';
export const EDIT_MESSAGE_FAILURE = 'EDIT_MESSAGE_FAILURE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const DELETE_MESSAGE_SUCCESS = 'DELETE_MESSAGE_SUCCESS';
export const DELETE_MESSAGE_FAILURE = 'DELETE_MESSAGE_FAILURE';
export const LIKE_MESSAGE = 'LIKE_MESSAGE';
export const LIKE_MESSAGE_SUCCESS = 'LIKE_MESSAGE_SUCCESS';
export const LIKE_MESSAGE_FAILURE = 'LIKE_MESSAGE_FAILURE';
export const DISLIKE_MESSAGE = 'DISLIKE_MESSAGE';
export const DISLIKE_MESSAGE_SUCCESS = 'DISLIKE_MESSAGE_SUCCESS';
export const DISLIKE_MESSAGE_FAILURE = 'DISLIKE_MESSAGE_FAILURE';

export interface IFetchMessagesAction {
  type: typeof FETCH_MESSAGES  
}

export interface IFetchtMessagesSuccessAction {
  type: typeof FETCH_MESSAGES_SUCCESS
  payload: IMessageConfig[]
}

export interface IFetchtMessagesFailureAction {
  type: typeof FETCH_MESSAGES_FAILURE
  payload: IHttpError
}

export interface IAddMessageAction {
  type: typeof ADD_MESSAGE
  payload: IMessageConfig
}

export interface IAddMessageSuccessAction {
  type: typeof ADD_MESSAGE_SUCCESS  
}

export interface IAddMessageFailureAction {
  type: typeof ADD_MESSAGE_FAILURE
  payload: IHttpError
}

export interface ISearchMessagesAction {
  type: typeof SEARCH_MESSAGES
  payload: ISearchElement
}

export interface ISearchMessagesResultAction {
  type: typeof SEARCH_MESSAGES_RESULT
  payload: IMessageConfig[]
}

export interface ISearchMessagesFailureAction {
  type: typeof SEARCH_MESSAGES_FAILURE
  payload: IHttpError
}

export interface IEditMessageAction {
  type: typeof EDIT_MESSAGE
  payload: IMessageConfig
}

export interface IEditMessageSuccessAction {
  type: typeof EDIT_MESSAGE_SUCCESS  
}

export interface IEditMessageFailureAction {
  type: typeof EDIT_MESSAGE_FAILURE
  payload: IHttpError
}

export interface IDeleteMessageAction {
  type: typeof DELETE_MESSAGE
  payload: string
}

export interface IDeleteMessageSuccessAction {
  type: typeof DELETE_MESSAGE_SUCCESS  
}

export interface IDeleteMessageFailureAction {
  type: typeof DELETE_MESSAGE_FAILURE
  payload: IHttpError
}

export interface ILikeMessageAction {
  type: typeof LIKE_MESSAGE
  payload: IMessageReact
}

export interface ILikeMessageSuccessAction {
  type: typeof LIKE_MESSAGE_SUCCESS  
}

export interface ILikeMessageFailureAction {
  type: typeof LIKE_MESSAGE_FAILURE
  payload: IHttpError
}

export interface IDislikeMessageAction {
  type: typeof DISLIKE_MESSAGE
  payload: IMessageReact
}

export interface IDislikeMessageSuccessAction {
  type: typeof DISLIKE_MESSAGE_SUCCESS  
}

export interface IDislikeMessageFailureAction {
  type: typeof DISLIKE_MESSAGE_FAILURE
  payload: IHttpError
}

export type MessageActionTypes = 
  IFetchMessagesAction |
  IFetchtMessagesSuccessAction |
  IFetchtMessagesFailureAction |
  IAddMessageAction | 
  IAddMessageSuccessAction |
  IAddMessageFailureAction |
  ISearchMessagesAction |
  ISearchMessagesResultAction | 
  ISearchMessagesFailureAction |   
  IEditMessageAction |
  IEditMessageSuccessAction |
  IEditMessageFailureAction |
  IDeleteMessageAction |
  IDeleteMessageSuccessAction |
  IDeleteMessageFailureAction |
  ILikeMessageAction |
  ILikeMessageSuccessAction |
  ILikeMessageFailureAction |
  IDislikeMessageAction |
  IDislikeMessageSuccessAction |
  IDislikeMessageFailureAction;