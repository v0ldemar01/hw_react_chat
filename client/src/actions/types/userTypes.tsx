import IUser from '../../models/user';
import IHttpError from '../../models/error';

export const LOAD_USER = 'LOAD_USER';
export const LOAD_USER_SUCCESS = 'LOAD_USER_SUCCESS';
export const LOAD_USER_FAILURE = 'LOAD_USER_FAILURE';
export const LOGIN_INIT = 'LOGIN_INIT';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const REGISTER_INIT = 'REGISTER_INIT';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';
export const ADD_USER = 'ADD_USER';
export const ADD_USER_SUCCESS = 'ADD_USER_SUCCESS';
export const ADD_USER_FAILURE = 'ADD_USER_FAILURE';
export const EDIT_USER = 'EDIT_USER';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';
export const EDIT_USER_FAILURE = 'EDIT_USER_FAILURE';
export const DELETE_USER = 'DELETE_USER';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const DELETE_USER_FAILURE = 'EDIT_USER_FAILURE';
export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';

export interface ILoadUserAction {
  type: typeof LOAD_USER  
}

export interface ILoginInitAction {
  type: typeof LOGIN_INIT
  payload: IUser
}

export interface IRegisterInitAction {
  type: typeof REGISTER_INIT
  payload: IUser
}

export interface ILoadUserSuccessAction {
  type: typeof LOAD_USER_SUCCESS
  payload: IUser
}

export interface ILoginSuccessAction {
  type: typeof LOGIN_SUCCESS
  payload: IUser
}

export interface IRegisterSuccessAction {
  type: typeof REGISTER_SUCCESS
  payload: IUser
}

export interface ILoadUserFailureAction {
  type: typeof LOAD_USER_FAILURE
  payload: IHttpError
}

export interface ILoginFailureAction {
  type: typeof LOGIN_FAILURE
  payload: IHttpError
}

export interface IRegisterFailureAction {
  type: typeof REGISTER_FAILURE
  payload: IHttpError
}

export interface IAddUserAction {
  type: typeof ADD_USER
  payload: IUser
}

export interface IAddUserSuccessAction {
  type: typeof ADD_USER_SUCCESS
}

export interface IAddUserFailureAction {
  type: typeof ADD_USER_FAILURE
  payload: IHttpError
}

export interface IEditUserAction {
  type: typeof EDIT_USER
  payload: IUser
}

export interface IEditUserSuccessAction {
  type: typeof EDIT_USER_SUCCESS
}

export interface IEditUserFailureAction {
  type: typeof EDIT_USER_FAILURE
  payload: IHttpError
}

export interface IDeleteUserAction {
  type: typeof DELETE_USER
  payload: string
}

export interface IDeleteUserSuccessAction {
  type: typeof DELETE_USER_SUCCESS
}

export interface IDeleteUserFailureAction {
  type: typeof DELETE_USER_FAILURE
  payload: IHttpError
}

export interface IFetchUsersAction {
  type: typeof FETCH_USERS
}

export interface IFetchUsersSuccessAction {
  type: typeof FETCH_USERS_SUCCESS,
  payload: IUser[]
}

export interface IFetchUsersFailureAction { 
  type: typeof FETCH_USERS_FAILURE,
  payload: IHttpError
}

export type UserActionTypes =   
  ILoadUserAction |
  ILoginInitAction | 
  IRegisterInitAction |
  ILoadUserSuccessAction |
  ILoginSuccessAction |
  IRegisterSuccessAction | 
  ILoadUserFailureAction |
  ILoginFailureAction | 
  IRegisterFailureAction |
  IAddUserAction |
  IAddUserSuccessAction |
  IAddUserFailureAction |
  IEditUserAction |
  IEditUserSuccessAction |
  IEditUserFailureAction |
  IDeleteUserAction | 
  IDeleteUserSuccessAction | 
  IDeleteUserFailureAction | 
  IFetchUsersAction | 
  IFetchUsersSuccessAction |
  IFetchUsersFailureAction;