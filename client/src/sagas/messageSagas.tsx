import { call, put, takeEvery, all } from 'redux-saga/effects';
import axios from 'axios';
import {url} from '../config/api';
import { 
  FETCH_MESSAGES,
  ADD_MESSAGE,
  SEARCH_MESSAGES,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
	DISLIKE_MESSAGE
} from '../actions/types/messageTypes';
import * as messageTypes from '../actions/types/messageTypes';
import {
  fetchMessagesSuccess,
  fetchMessagesFailure,  
  addMessageSuccess,
  addMessageFailure, 
  searchMessagesResult,
  searchMessagesFailure, 
  editMessageSuccess,
  editMessageFailure,  
  deleteMessageSuccess,
  deleteMessageFailure,
  likeMessageSuccess,
  likeMessageFailure,
	dislikeMessageSuccess,
	dislikeMessageFailure
} from '../actions/messageActions';
import { getHeaders } from '../helpers/apiHelper';

const buildURLQuery = (obj: any) =>
	Object.entries(obj)
		.map(pair => pair.map((element: any) => encodeURIComponent(element)).join('='))
		.join('&');

function* fetchMessagesWorker() {
	try {			
		const messages = yield call(axios.get, `${url}/api/messages/`, {headers: getHeaders()});		
		yield put(fetchMessagesSuccess(messages.data));		
	} catch (error) {
		console.log('fetchMessagesFailure', error);
    yield put(fetchMessagesFailure(error.message));	
	}
}

function* watchLoginUser() {
	yield takeEvery(FETCH_MESSAGES, fetchMessagesWorker)
}

function* addMessageWorker(action: messageTypes.IAddMessageAction) {
	try {
		yield call(axios.post, `${url}/api/messages/`, action.payload, {headers: getHeaders() });
		yield put(addMessageSuccess());
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
    yield put(addMessageFailure(error.message));		
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessageWorker)
}

function* searchMessagesWorker(action: messageTypes.ISearchMessagesAction) {
	try {		
		const messages = yield call(axios.get, `${url}/api/messages?${buildURLQuery(action.payload)}`, {headers: getHeaders()});		
		yield put(searchMessagesResult(messages.data));
	} catch (error) {
    yield put(searchMessagesFailure(error.message));		
	}
}

function* watchSearchMessages() {
	yield takeEvery(SEARCH_MESSAGES, searchMessagesWorker)
}

export function* editMessageWorker(action: messageTypes.IEditMessageAction) {
	const id = action.payload.id;
	const updatedUser = { ...action.payload };	
	try {
		yield call(axios.put, `${url}/api/messages/${id}`, updatedUser, {headers: getHeaders()} );
    yield put(editMessageSuccess());
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
    yield put(editMessageFailure(error.message));				
	}
}

function* watchEditMessage() {
	yield takeEvery(EDIT_MESSAGE, editMessageWorker)
}

export function* deleteMessageWorker(action: messageTypes.IDeleteMessageAction) {
	try {
		yield call(axios.delete, `${url}/api/messages/${action.payload}`, {headers: getHeaders()});
    yield put(deleteMessageSuccess());
		yield put({ type: FETCH_MESSAGES })
	} catch (error) {
		yield put(deleteMessageFailure(error.message));
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessageWorker)
}

export function* likeMessageWorker(action: messageTypes.ILikeMessageAction) {
	try {
		yield call(axios.put, `${url}/api/messages/react`, {...action.payload, isLike: true}, {headers: getHeaders()});
    yield put(likeMessageSuccess());
		yield put({ type: FETCH_MESSAGES })
	} catch (error) {
		yield put(likeMessageFailure(error.message));
	}
}

function* watchLikeMessage() {
	yield takeEvery(LIKE_MESSAGE, likeMessageWorker)
}

export function* dislikeMessageWorker(action: messageTypes.IDislikeMessageAction) {
	try {
		yield call(axios.put, `${url}/api/messages/react`, {...action.payload, isDisLike: true}, {headers: getHeaders()});
    yield put(dislikeMessageSuccess());
		yield put({ type: FETCH_MESSAGES })
	} catch (error) {
		yield put(dislikeMessageFailure(error.message));
	}
}

function* watchDislikeMessage() {
	yield takeEvery(DISLIKE_MESSAGE, dislikeMessageWorker)
}

export default function* messageSagas() {
	yield all([
		watchLoginUser(),
		watchAddMessage(),
		watchSearchMessages(),
		watchEditMessage(),
		watchDeleteMessage(),
		watchLikeMessage(),
		watchDislikeMessage()
	]);
};