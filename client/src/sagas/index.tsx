import { all } from 'redux-saga/effects';
import messagesSagas from './messageSagas';
import usersSagas from './userSagas';

export default function* rootSaga() {
  yield all([
    messagesSagas(),
    usersSagas()
  ]);
};