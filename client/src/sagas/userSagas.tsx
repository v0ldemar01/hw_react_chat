import axios from 'axios';
import {url} from '../config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { 
	LOAD_USER,
  LOGIN_INIT,
  REGISTER_INIT,
  ADD_USER,
  EDIT_USER,
  DELETE_USER,
  FETCH_USERS,
	ILoginInitAction,
	IRegisterInitAction,
	IAddUserAction,
	IEditUserAction,
	IDeleteUserAction
} from '../actions/types/userTypes';
import {showLoader, hideLoader } from '../actions/mainActions';
import {
	loadUserSuccess,
	loadUserFailure,
  loginSuccess,
  loginFailure,  
  registerSuccess,
  registerFailure, 
  addUserSuccess,
  addUserFailure, 
  editUserSuccess,
  editUserFailure,  
  deleteUserSuccess,
  deleteUserFailure,
  fetchUsersSuccess,
  fetchUsersFailure
} from '../actions/userActions';
import {
	fetchMessages
} from '../actions/messageActions';
import { getHeaders } from '../helpers/apiHelper';

const setToken = (token: string) => localStorage.setItem('token', token);

function* loadUserWorker() {
	try {
		yield put(showLoader());
		const {data} = yield call(axios.get, `${url}/api/auth/user`, {headers: getHeaders()});	
		yield put(loadUserSuccess(data))	
		yield put(fetchMessages());
		yield put({ type: FETCH_USERS });
	} catch (error) {
    yield put(loadUserFailure(error.message));				
	} finally {
		yield put(hideLoader());
	}
}

function* watchLoadUser() {
	yield takeEvery(LOAD_USER, loadUserWorker)
}

function* loginUserWorker(action: ILoginInitAction) {
	try {
		yield put(showLoader());
		const {data} = yield call(axios.post, `${url}/api/auth/login`, action.payload);
		setToken(data.token);
		yield put(loginSuccess(data.user));		
		yield put(fetchMessages());
		yield put({ type: FETCH_USERS });
	} catch (error) {
    yield put(loginFailure(error.message));		
	} finally {
		yield put(hideLoader());
	}
}

function* watchLoginUser() {
	yield takeEvery(LOGIN_INIT, loginUserWorker)
}

function* registerUserWorker(action: IRegisterInitAction) {
	try {
		yield put(showLoader());
		const {data} = yield call(axios.post, `${url}/api/auth/register`, action.payload);
		setToken(data.token);
		yield put(registerSuccess(data.user));
		yield put(fetchMessages());
		yield put({ type: FETCH_USERS });
		yield put(hideLoader());
	} catch (error) {
    yield put(registerFailure(error.message));		
	}
}

function* watchRegisterUser() {
	yield takeEvery(REGISTER_INIT, registerUserWorker)
}

function* fetchUsersWorker() {
	try {
		// yield put(showLoader());
		const users = yield call(axios.get, `${url}/api/users`, {headers: getHeaders()});
		yield put(fetchUsersSuccess(users.data));		
	} catch (error) {
    yield put(fetchUsersFailure(error.message));		
	} finally {
		// yield put(hideLoader());
	}
}

function* watchFetchUsers() {
	yield takeEvery(FETCH_USERS, fetchUsersWorker)
}

function* addUserWorker(action: IAddUserAction) {
	try {
		yield call(axios.post, `${url}/api/users`, action.payload, {headers: getHeaders()});
    yield put(addUserSuccess());
		yield put({ type: FETCH_USERS });
	} catch (error) {
    yield put(addUserFailure(error.message));		
	}
}

function* watchAddUser() {
	yield takeEvery(ADD_USER, addUserWorker)
}

export function* editUserWorker(action: IEditUserAction) {
	const id = action.payload.id;
	const updatedUser = { ...action.payload };	
	try {
		yield call(axios.put, `${url}/api/users/${id}`, updatedUser, {headers: getHeaders()});
    yield put(editUserSuccess());
		yield put({ type: FETCH_USERS });
	} catch (error) {
    yield put(editUserFailure(error.message));				
	}
}

function* watchEditUser() {
	yield takeEvery(EDIT_USER, editUserWorker)
}

export function* deleteUserWorker(action: IDeleteUserAction) {
	try {
		yield call(axios.delete, `${url}/api/users/${action.payload}`, {headers: getHeaders()});
    yield put(deleteUserSuccess());
		yield put({ type: FETCH_USERS })
	} catch (error) {
		yield put(deleteUserFailure(error.message));
	}
}

function* watchDeleteUser() {
	yield takeEvery(DELETE_USER, deleteUserWorker)
}

export default function* userSagas() {
	yield all([
		watchLoadUser(),
    watchLoginUser(),
    watchRegisterUser(),
		watchFetchUsers(),
		watchAddUser(),
		watchEditUser(),
		watchDeleteUser()
	])
};
