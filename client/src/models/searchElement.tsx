export default interface ISearchElement {
  searchTerm: string,
  searchLoading: boolean
}