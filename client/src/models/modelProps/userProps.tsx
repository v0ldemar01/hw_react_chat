import IUser from '../user';

export default interface IUserProps {
  user: IUser,
  editUser: Function,
  deleteUser: Function
}