import IMessageConfig from '../message';
import IUser from '../user';

export default interface IMessageProps {
  message: IMessageConfig, 
  currentUser: IUser, 
  likeMessage: Function,
  dislikeMessage: Function,
  deleteMessage: Function, 
  isLast: boolean
}