import IMessageConfig from '../message';
import IUser from '../user';

export default interface IMessageListProps {
  myUser: IUser,
  messages: IMessageConfig[],
}