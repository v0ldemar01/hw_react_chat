import IUser from '../user';

export default interface IMessageInputProps {
  myUser: IUser,
  handleNewMessage: Function
}