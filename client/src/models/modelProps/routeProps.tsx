import React from 'react';
import { RouteProps } from 'react-router';

export default interface IRouteProps {
  component: React.FunctionComponent<any>,
  props?: RouteProps, 
  exact?: boolean,
  path: string
}
