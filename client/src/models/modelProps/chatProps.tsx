import IMessageConfig from '../message';
import ISearchElement from '../searchElement';
import IUser from '../user';

export default interface IChatProps {
  setLoading: Function,
  myUser: IUser,
  search: ISearchElement,
  messages: IMessageConfig[],
  filterMessages: IMessageConfig[],
  getMessages: Function,
  addMessage: Function,
  searchMessages: Function,
  searchMessagesResult: Function
}