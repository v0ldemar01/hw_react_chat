export default interface IChatHeaderProps {
  channelName: string,
  handleSearchChange: Function,
  searchLoading: boolean,
  usersCount: number,
  messagesCount: number,
  lastMessageTime: string
}