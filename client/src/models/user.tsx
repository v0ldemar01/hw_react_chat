export default interface IUser {  
  id?: string,
  avatar?: string,
  username: string,
  password?: string,
  isAdmin?: boolean,
  createdAt?: string,
  updatedAt?: string
}