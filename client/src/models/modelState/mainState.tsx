export default interface IMainState {
  isLoading: boolean
  error?: string
}