import IMessageConfig from '../message';
import ISearchElement from '../searchElement';

export default interface IMessageState {  
  messages: IMessageConfig[],
  filterMessages: IMessageConfig[],  
  search: ISearchElement,
  error?: string 
}