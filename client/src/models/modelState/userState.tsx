import IUser from '../user';

export default interface IUserState {  
  currentUser: IUser
  isLoggedIn: boolean,
  users?: IUser[],
  error?: string
}