import IMessageState from './messageState';
import IMessagePage from './messagePageState';
import IMainState from './mainState';
import IUserState from './userState';

export default interface IState {
  message: IMessageState,
  messagePage: IMessagePage,
  main: IMainState,
  user: IUserState
}