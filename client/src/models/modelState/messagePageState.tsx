export default interface IMessagePageState {
  isShown: boolean,
  error?: string
}