import IUser from './user';

export default interface IMessageConfig extends IUser {
  id: string,  
  text: string,
  createdAt: string,
  editedAt: string,
  likeCount?: string,
  dislikeCount?: string,
  user: IUser,
  userId: string,
  action?: string
}